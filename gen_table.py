from parse_dcode_in import parse_dcode

def escape(c):
    if c in ['\\', '-', '_', '&', '|']:
        return '\\' + c
    return c

dct, _ = parse_dcode()

out = ''

num_cols = 6
out = '| Símbolo | Letra ' * num_cols + '|\n'
out += '|:-------:|:-----:' * num_cols + '|\n'

i = 0
for k, v in dct.items():
    out += f'|![img](images/char{k}.png)|{escape(v)}'
    i = (i + 1) % num_cols

    if i == 0:
        out += '|\n'

if i != 0:
    out += '||' * (num_cols - i)
    out += '|\n'
    
print(out)

