import sys
import re
from cor import Colors 

with open(sys.argv[1]) as f:
    texto = f.read()

part = sys.argv[2]

#i = texto.find(part)
i = re.search(part, texto)

while i is not None:
    j, i = i.end(), i.start()
    
    print(texto[:i], end='')
    print(Colors.RED + texto[i:j], end=Colors.END)

    texto = texto[j:]
    i = re.search(part, texto)

print(texto)
    
