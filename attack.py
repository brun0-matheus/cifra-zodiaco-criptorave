import sys
import re
from cor import Colors 

dct = {
    'k': 'A', 'm': 'A', '3': 'A',
    'z': 'C',
    'h': 'D', 't' :'D', '$': 'H',
    'a': 'E', 'i': 'E', 'o': 'E', '2': 'E', '%': 'E',
    ',': 'F',
    '1': 'H',
    '-': 'I', 'u': 'I', 'x': 'I', '4': 'I', 
    '#': 'L', '+': 'L', '0': 'L',
    'j': 'M',
    'p': 'N', '/': 'N', '|': 'N', '&': 'N',
    'y': 'O',
    'q': 'P',
    's': 'Q',
    'b': 'U',

    'n': 'R', 'r': 'R', 'w': 'R',

    'c': 'G',
    'd': 'O',
    'e': 'S',
    'f': 'T', 'l': 'T',
    'g': 'O',

    'v': 'V',
    #'c': 'Q',
    #'d': 'U',
    #'e': 'E',
    #'f': 'R',
    #'g': 'O'
}

with open(sys.argv[1]) as f:
    texto = f.read().lower()

for k, v in dct.items():
    texto = texto.replace(k.lower(), v.upper())

if len(sys.argv) == 2:
    print(texto)
    exit()
    
part = sys.argv[2]

i = re.search(part, texto)

while i is not None:
    j, i = i.end(), i.start()
    
    print(texto[:i], end='')
    print(Colors.RED + texto[i:j], end=Colors.END)

    texto = texto[j:]
    i = re.search(part, texto)

print(texto)

