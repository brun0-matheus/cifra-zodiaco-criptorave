from string import ascii_lowercase


def parse_dcode():
    with open('in_dcode.txt') as f:
        a = f.read()

    a = a.replace('char(', '').strip()
    nums = map(int, filter(lambda x: len(x) > 0, a.split(')')))

    alpha = ascii_lowercase + '0123456789' + '!@#$%&*+=/?|^~-<>,.;:'

    assert len(set(alpha)) == len(alpha)

    dct = {}
    i = 0

    out = ''
    for n in nums:
        if n not in dct:
            dct[n] = alpha[i]
            i += 1

        out += dct[n]

    return dct, out
    

if __name__ == '__main__':
    dct, out = parse_dcode()

    print(out)
    #print('\nInverse map:')

    #for k, v in dct.items():
    #    print(f'{k} = {v}', end=', ')

    rev = {v: k for k, v in dct.items()}

    #print('')
    
